package com.example.classhelp;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class AppSingleton  {
    private static AppSingleton INSTANCE = null;
    private static SharedPreferences preferences;


    private AppSingleton() {

    }

    public static AppSingleton getInstance(Context context){
        if(INSTANCE == null){
            INSTANCE = new AppSingleton();
        }
        if(preferences == null){
            preferences = context.getSharedPreferences(context.getPackageName(), Activity.MODE_PRIVATE);
        }
        return INSTANCE;
    }

    public String getOldtoken(){
        return preferences.getString("old_token", "");
    }

    public void setNewToken(String token){
        preferences.edit().putString("new_token", token).commit();
    }

    public String getNewToken(){
        return preferences.getString("new_token", "");
    }

    public  void setOldToken(String token){
        preferences.edit().putString("old_token", token).commit();
    }

    public void deleteNewToken() {
        preferences.edit().putString("new_token", "").commit();
    }

    public void checkToken(String uid){
        if(!getNewToken().equals("")){
            addOrChangeToken(uid);
        }
    }

    public void addOrChangeToken(String uid){
        DatabaseReference dRef = FirebaseDatabase.getInstance().getReference("users").child(uid).child("tokens");
        if(getOldtoken().equals("")){
            dRef.child(getNewToken()).setValue(true);
        }else{
            dRef.child(getOldtoken()).removeValue();
            dRef.child(getNewToken()).setValue(true);
        }
        setOldToken(getNewToken());
        deleteNewToken();
    }
}
