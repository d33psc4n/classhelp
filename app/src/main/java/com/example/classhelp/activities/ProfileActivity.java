package com.example.classhelp.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.classhelp.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mikhaellopez.circularimageview.CircularImageView;

public class ProfileActivity extends AppCompatActivity {
    CircularImageView profile;
    EditText phone, name;
    Button buttonCall;
    private static final int PERMISSION_CHECK = 5;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_CHECK) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(ProfileActivity.this, "Permission granted.", Toast.LENGTH_SHORT).show();
            }else{
               Toast.makeText(ProfileActivity.this, "You won't be able to make calls if you don't grant permission.", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void requestCallPermission(){
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.CALL_PHONE}, PERMISSION_CHECK);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {
           requestCallPermission();
        }

        setTitle("Profile");


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        String uid = "";
        if (getIntent().getExtras() != null)
            uid = getIntent().getExtras().getString("uid", "");

        profile = findViewById(R.id.imgViewProfile);
        phone = findViewById(R.id.editPhone);
        name = findViewById(R.id.editName);
        buttonCall = findViewById(R.id.buttonCall);

        buttonCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!phone.getText().toString().equals("")) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + phone.getText().toString()));
                    if (ActivityCompat.checkSelfPermission(ProfileActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        requestCallPermission();
                        return;
                    }
                    startActivity(callIntent);
                }
           }
       });

        DatabaseReference user = FirebaseDatabase.getInstance().getReference("users").child(uid);
        user.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Glide.with(ProfileActivity.this).load(dataSnapshot.child("profilePicture").getValue().toString()).into(profile);
                phone.setText(dataSnapshot.child("phoneNumber").getValue().toString());
                String nameSnap = dataSnapshot.child("displayName").getValue().toString();
                name.setText(nameSnap);
                ProfileActivity.this.setTitle(nameSnap);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
