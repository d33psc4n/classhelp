package com.example.classhelp.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.example.classhelp.AppSingleton;
import com.example.classhelp.R;
import com.example.classhelp.fragments.GiveFragment;
import com.example.classhelp.fragments.HelpersFragment;
import com.example.classhelp.fragments.NeedFragment;
import com.example.classhelp.fragments.ProfileFragment;
import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.io.IOException;


public class MainActivity extends AppCompatActivity {

    private BottomNavigationView navigation;
    private DatabaseReference mDb;
    private boolean goToHelpers = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bottom_navigation_light);
        if(getIntent().getExtras() != null)
            goToHelpers = getIntent().getExtras().getString("helper", "").equals("true");
        mDb = FirebaseDatabase.getInstance().getReference("users");
        checkLogin();
        initComponent();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_top, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == R.id.logout){
            AppSingleton.getInstance(this).deleteNewToken();
            AuthUI.getInstance()
                    .signOut(this)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        public void onComplete(@NonNull Task<Void> task) {
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        FirebaseInstanceId.getInstance().deleteInstanceId();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }).start();
                            checkLogin();

                        }
            });
        }
        return super.onOptionsItemSelected(item);
    }

    private void goToLogin(){
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void checkLogin(){
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if(currentUser == null){
            goToLogin();
        }else{
            AppSingleton.getInstance(this).checkToken(currentUser.getUid());
            mDb = mDb.child(currentUser.getUid());
            mDb.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if(dataSnapshot.getValue() == null){
                        goToLogin();
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
    }

    private void initComponent() {
        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        Fragment initial = goToHelpers ? new HelpersFragment() : new NeedFragment();

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, initial)
                .commit();

        if(goToHelpers) navigation.getMenu().getItem(2).setChecked(true);

        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Fragment fragment = null;
                switch (item.getItemId()) {
                    case R.id.navigation_need:
                        fragment = new NeedFragment();
                        break;
                    case R.id.navigation_give:
                        fragment = new GiveFragment();
                        break;
                    case R.id.navigation_helpers:
                        fragment = new HelpersFragment();
                        break;
                    case R.id.navigation_profile:
                        fragment = new ProfileFragment();
                        break;
                }
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragment_container, fragment)
                        .commit();
                return true;
            }
        });


    }
}
