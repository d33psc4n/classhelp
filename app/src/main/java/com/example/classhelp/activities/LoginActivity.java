package com.example.classhelp.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.example.classhelp.R;
import com.example.classhelp.model.User;
import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class LoginActivity extends AppCompatActivity {

    private final int PICK_IMAGE_REQUEST = 22;
    private FirebaseStorage storage;
    private StorageReference storageReference;
    private int RC_SIGN_IN = 1;
    private List<AuthUI.IdpConfig> providers;
    private Button buttonLogin, buttonCreateProfile;
    private FloatingActionButton fab;
    private LinearLayout linearLayout;
    private CircularImageView imageViewProfile;
    private EditText editName, editPhone;
    private FirebaseUser user;
    private DatabaseReference mDatabase;
    private Uri filePath;
    private String phoneNumber, name;
    private String profilePhoto = "";

    private DatabaseReference userProfileRef;
    private User userProfile;

    @SuppressLint("RestrictedApi")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            IdpResponse response = IdpResponse.fromResultIntent(data);
            if (resultCode == RESULT_OK) {
                user = FirebaseAuth.getInstance().getCurrentUser();
                setOnlineDataAndShowForm();
            } else {
                Toast.makeText(this, "Sign in was canceled. Please try again.", Toast.LENGTH_LONG).show();
                buttonLogin.setVisibility(View.VISIBLE);
            }
        }
        if (requestCode == PICK_IMAGE_REQUEST
                && resultCode == RESULT_OK
                && data != null
                && data.getData() != null) {
            filePath = data.getData();
            uploadImage();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setTitle("Login or Signup");

        mDatabase = FirebaseDatabase.getInstance().getReference("users");

        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();

        buttonLogin = findViewById(R.id.buttonLogin);
        buttonCreateProfile = findViewById(R.id.buttonCall);

        linearLayout = findViewById(R.id.linearLayoutMain);
        fab = findViewById(R.id.fab);
        editName = findViewById(R.id.editName);
        editPhone = findViewById(R.id.editPhone);
        imageViewProfile = findViewById(R.id.imgViewProfile);

        providers = Collections.singletonList(
                new AuthUI.IdpConfig.GoogleBuilder().build());

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startLogin();
            }
        });
        user = FirebaseAuth.getInstance().getCurrentUser();
        if (user == null) {
            startLogin();
        } else {
            setOnlineDataAndShowForm();
        }

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });

        buttonCreateProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                phoneNumber = editPhone.getText().toString();
                name = editName.getText().toString();
                if (phoneNumber.isEmpty() || name.isEmpty()) {
                    Toast.makeText(LoginActivity.this, "Please check the form", Toast.LENGTH_LONG).show();
                } else {
                    addUserToFirebase();
                }
            }
        });
    }

    private void setOnlineDataAndShowForm(){
        userProfileRef = FirebaseDatabase.getInstance().getReference("users").child(user.getUid());
        ValueEventListener userListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                userProfile = dataSnapshot.getValue(User.class);
                if(userProfile == null){
                    editName.setText(user.getDisplayName());
                    editPhone.setText(user.getPhoneNumber());
                    Glide.with(LoginActivity.this).load(user.getPhotoUrl()).into(imageViewProfile);
                    showForm();
                }else{
                    goToMain();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w("Firebase",  databaseError.toException());
            }
        };
        userProfileRef.addValueEventListener(userListener);
    }

    private void selectImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(
                Intent.createChooser(
                        intent,
                        "Select Image from here..."),
                PICK_IMAGE_REQUEST);
    }

    private void goToMain(){
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void addUserToFirebase() {
        String id = user.getUid();
        if(profilePhoto.equals("")){
            profilePhoto = user.getPhotoUrl().toString();
        }
        User user = new User(name, profilePhoto, phoneNumber);
        mDatabase.child(id).setValue(user).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                goToMain();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(LoginActivity.this, "An error occured. Try again", Toast.LENGTH_SHORT);
            }
        });
    }

    @SuppressLint("RestrictedApi")
    private void showForm() {
        buttonLogin.setVisibility(View.GONE);
        linearLayout.setVisibility(View.VISIBLE);
        fab.setVisibility(View.VISIBLE);
    }

    private void startLogin() {
        startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(providers)
                        .build(),
                RC_SIGN_IN);
    }

    private void uploadImage() {
        if(filePath != null){
            final StorageReference ref = storageReference.child("images/" + UUID.randomUUID().toString());
            UploadTask uploadTask = ref.putFile(filePath);
            Task<Uri> urlTrask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if(!task.isSuccessful()) throw task.getException();
                    return ref.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if(task.isSuccessful()){
                        profilePhoto = task.getResult().toString();
                        Glide.with(LoginActivity.this).load(profilePhoto).into(imageViewProfile);
                    }else{
                        Toast.makeText(LoginActivity.this, "Image upload failed.", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }
}
