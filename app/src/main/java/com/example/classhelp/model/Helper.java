package com.example.classhelp.model;

public class Helper {
    private String profilePhoto;
    private String name;
    private String subject;
    private String uid;

    public Helper(){

    }

    public Helper(String profilePhoto, String name, String subject, String uid) {
        this.profilePhoto = profilePhoto;
        this.name = name;
        this.subject = subject;
        this.uid = uid;
    }

    public String getProfilePhoto() {
        return profilePhoto;
    }

    public void setProfilePhoto(String profilePhoto) {
        this.profilePhoto = profilePhoto;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}
