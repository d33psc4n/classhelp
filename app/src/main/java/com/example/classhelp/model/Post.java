package com.example.classhelp.model;

import java.util.List;
import java.util.Map;

public class Post {
    private String datetime;
    private int hoursNeeded;
    private String subject;
    private String description;
    private String uid;
    private Map<String, String> timestamp;
    private List<String> helpers;
    private String id;
    private long invertedTimestamp;

    public Post(){

    }


    public Post(String datetime, int hoursNeeded, String subject, String description, Map<String, String> timestamp) {
        this.datetime = datetime;
        this.hoursNeeded = hoursNeeded;
        this.timestamp = timestamp;
        this.subject = subject;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<String> getHelpers(){
        return helpers;
    }

    public void setHelpers(List<String> helpers){
        this.helpers = helpers;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public Map<String, String> getTimestamp() {
        return timestamp;
    }

    public long getInvertedTimestamp() {
        return invertedTimestamp;
    }

    public void setInvertedTimestamp(long invertedTimestamp) {
        this.invertedTimestamp = invertedTimestamp;
    }

    public void setTimestamp(Map<String, String> timestamp) {
        this.timestamp = timestamp;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public int getHoursNeeded() {
        return hoursNeeded;
    }

    public void setHoursNeeded(int hoursNeeded) {
        this.hoursNeeded = hoursNeeded;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
