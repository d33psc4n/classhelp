package com.example.classhelp.model;

public class User {
    private String displayName;
    private String profilePicture;
    private String phoneNumber;

    public User(){

    }

    public User(String displayName, String profilePicture, String phoneNumber) {
        this.displayName = displayName;
        this.profilePicture = profilePicture;
        this.phoneNumber = phoneNumber;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
