package com.example.classhelp.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.classhelp.R;
import com.example.classhelp.model.User;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.UUID;

import static android.app.Activity.RESULT_OK;


public class ProfileFragment extends Fragment {

    private User userProfile;
    private EditText editPhone, editName;
    private Button saveButton;
    private String photoUrl;
    private CircularImageView imageView;
    private final int PICK_IMAGE_REQUEST = 22;
    private Uri filePath;
    private StorageReference storageReference;
    private String profilePhoto = "";
    private FirebaseStorage storage;
    private FirebaseUser currentUser;

    public ProfileFragment() {
        // Required empty public constructor
    }

    public static ProfileFragment newInstance(String param1, String param2) {
        ProfileFragment fragment = new ProfileFragment();
        return fragment;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == PICK_IMAGE_REQUEST
                && resultCode == RESULT_OK
                && data != null
                && data.getData() != null) {
            filePath = data.getData();
            uploadImage();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root_view = inflater.inflate(R.layout.fragment_profile, container, false);

        getActivity().setTitle("Profile");

        currentUser = FirebaseAuth.getInstance().getCurrentUser();
        final DatabaseReference userProfileRef = FirebaseDatabase.getInstance().getReference("users").child(currentUser.getUid());
        editName = root_view.findViewById(R.id.editName);
        editPhone = root_view.findViewById(R.id.editPhone);
        saveButton = root_view.findViewById(R.id.buttonCall);
        imageView = root_view.findViewById(R.id.imgViewProfile);
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
        FloatingActionButton fab = root_view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!editPhone.getText().toString().isEmpty() && !editName.getText().toString().isEmpty()){
                    saveProfile();
                }else{
                    Toast.makeText(getActivity(), "Please check the form.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        ValueEventListener userListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                userProfile = dataSnapshot.getValue(User.class);
                editName.setText(userProfile.getDisplayName());
                editPhone.setText(userProfile.getPhoneNumber());
                if(getActivity() != null)
                    Glide.with(getActivity()).load(userProfile.getProfilePicture()).into(imageView);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w("Firebase",  databaseError.toException());
            }
        };
        userProfileRef.addValueEventListener(userListener);

        return root_view;
    }

    private void saveProfile() {
        if(profilePhoto.equals("")){
            profilePhoto = userProfile.getProfilePicture();
        }
        User user = new User(editName.getText().toString(), profilePhoto, editPhone.getText().toString());
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("users").child(currentUser.getUid());
        mDatabase.setValue(user).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(getActivity(), "Profile saved.", Toast.LENGTH_SHORT).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getActivity(), "An error occured. Try again", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void selectImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(
                Intent.createChooser(
                        intent,
                        "Select Image from here..."),
                PICK_IMAGE_REQUEST);
    }

    private void uploadImage() {
        if(filePath != null){
            final StorageReference ref = storageReference.child("images/" + UUID.randomUUID().toString());
            UploadTask uploadTask = ref.putFile(filePath);
            Task<Uri> urlTrask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if(!task.isSuccessful()) throw task.getException();
                    return ref.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if(task.isSuccessful()){
                        profilePhoto = task.getResult().toString();
                        Glide.with(getActivity()).load(profilePhoto).into(imageView);
                    }else{
                        Toast.makeText(getActivity(), "Image upload failed.", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }
}
