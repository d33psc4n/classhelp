package com.example.classhelp.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.classhelp.R;
import com.example.classhelp.model.Post;
import com.example.classhelp.model.User;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.firebase.ui.database.SnapshotParser;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class GiveFragment extends Fragment {

    RecyclerView recyclerView;
    private View root_view;
    LinearLayoutManager linearLayoutManager;
    FirebaseUser currentUser;
    private FirebaseRecyclerAdapter adapter;

    public GiveFragment() {
        // Required empty public constructor
    }

    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    public static GiveFragment newInstance(String param1, String param2) {
        GiveFragment fragment = new GiveFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        root_view = inflater.inflate(R.layout.fragment_give, container, false);
        recyclerView = root_view.findViewById(R.id.recyclerView);
        currentUser = FirebaseAuth.getInstance().getCurrentUser();

        getActivity().setTitle("Help someone");


        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);


        fetch();

        return root_view;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtViewSubject, txtViewDesc, txtViewName, txtViewTimeAgo, txtViewTimeNeeded,
                         txtViewDateTime;
        private Button btnHelp;
        private DatabaseReference dbRef;
        private ValueEventListener valueEventListener;
        private CircularImageView profileImage;

        public ViewHolder(View itemView) {
            super(itemView);
            txtViewDesc = itemView.findViewById(R.id.textViewDesc);
            txtViewName = itemView.findViewById(R.id.textViewName);
            txtViewSubject = itemView.findViewById(R.id.textViewSubject);
            txtViewTimeAgo = itemView.findViewById(R.id.textViewTimeAgo);
            txtViewTimeNeeded = itemView.findViewById(R.id.textViewTimeNeeded);
            txtViewDateTime = itemView.findViewById(R.id.textViewDateTimeNeeded);
            btnHelp = itemView.findViewById(R.id.buttonHelp);
            profileImage = itemView.findViewById(R.id.profileImage);

        }

    }

    private void fetch() {
        Query query = FirebaseDatabase.getInstance()
                .getReference()
                .child("posts").orderByChild("invertedTimestamp").limitToFirst(30);

        FirebaseRecyclerOptions<Post> options =
                new FirebaseRecyclerOptions.Builder<Post>()
                        .setQuery(query, new SnapshotParser<Post>() {
                            @NonNull
                            @Override
                            public Post parseSnapshot(@NonNull DataSnapshot snapshot) {
                                Post post = new Post();
                                post.setDatetime(snapshot.child("datetime").getValue().toString());
                                post.setDescription(snapshot.child("description").getValue().toString());
                                post.setHoursNeeded(Integer.parseInt(snapshot.child("hoursNeeded").getValue().toString()));
                                post.setSubject(snapshot.child("subject").getValue().toString());
                                if(snapshot.child("helpers").getValue() != null){
                                    List<String> helperi = new ArrayList<>();
                                    for (DataSnapshot d : snapshot.child("helpers").getChildren()){
                                        helperi.add(d.getKey());
                                    }
                                    post.setHelpers(helperi);
                                }
                                post.setId(snapshot.getKey());
                                post.setUid(snapshot.child("uid").getValue().toString());
                                Map<String,String> timestamp = new HashMap<String,String>();
                                timestamp.put("timestamp", snapshot.child("timestamp").getValue().toString());
                                post.setTimestamp(timestamp);
                                return post;
                            }
                        })
                        .build();

        adapter = new FirebaseRecyclerAdapter<Post, ViewHolder>(options) {
            @Override
            public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_give, parent, false);

                return new ViewHolder(view);
            }

            @Override
            public void onViewDetachedFromWindow(@NonNull ViewHolder holder) {
                holder.dbRef.removeEventListener(holder.valueEventListener);
                super.onViewDetachedFromWindow(holder);
            }

            @Override
            protected void onBindViewHolder(final ViewHolder holder, final int position, final Post post) {

                holder.txtViewDesc.setText(post.getDescription());
                holder.txtViewSubject.setText(post.getSubject());
                holder.txtViewTimeNeeded.setText(String.valueOf(post.getHoursNeeded()) + " hours");
                holder.txtViewDateTime.setText(post.getDatetime());
                long timestampLong = Long.parseLong(post.getTimestamp().get("timestamp"));
                holder.txtViewTimeAgo.setText(getReadableTimestamp(System.currentTimeMillis() - timestampLong));
                holder.dbRef = FirebaseDatabase.getInstance().getReference("users").child(post.getUid());
                holder.valueEventListener = new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        User user = dataSnapshot.getValue(User.class);
                        holder.txtViewName.setText(user.getDisplayName());
                        if(getActivity() != null) {
                            Glide.with(getActivity()).load(user.getProfilePicture()).into(holder.profileImage);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                };
                holder.dbRef.addValueEventListener(holder.valueEventListener);
                boolean attachListener = true;
                if(post.getHelpers() != null){
                    if(post.getHelpers().contains(currentUser.getUid())){
                        attachListener = false;
                        holder.btnHelp.setVisibility(View.INVISIBLE);
                    }
                }
                if(post.getUid().equals(currentUser.getUid())){
                    attachListener = false;
                }
                if(attachListener) {
                    holder.btnHelp.setVisibility(View.VISIBLE);
                    holder.btnHelp.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            addHelper(post.getId(), currentUser.getUid());
                        }
                    });
                }
            }

        };
        recyclerView.setAdapter(adapter);
    }

    private void addHelper(String postId, String userId){
        DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference("posts").child(postId).child("helpers");
        dbRef.child(userId).setValue(true).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(getActivity(), "Helped user", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private String getReadableTimestamp(long millisRelative){
        long seconds = millisRelative / 1000;
        if(seconds > 60){
            long minutes = seconds / 60;
            if(minutes > 60){
                int hours = (int) (minutes / 60);
                return String.format("%d hours ago", hours);
            }else return String.format("%d minutes ago", minutes);
        }else return String.format("%d seconds ago", seconds);
    }
}
