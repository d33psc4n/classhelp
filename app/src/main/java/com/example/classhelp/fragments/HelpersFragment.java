package com.example.classhelp.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.balysv.materialripple.MaterialRippleLayout;
import com.bumptech.glide.Glide;
import com.example.classhelp.R;
import com.example.classhelp.activities.ProfileActivity;
import com.example.classhelp.model.Helper;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.ArrayList;
import java.util.List;

public class HelpersFragment extends Fragment {
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private View root_view;
    private List<Helper> helpers;
    private FirebaseDatabase mDb;
    private HelperAdapter mAdapter;
    private RelativeLayout relativeLayoutNoHelpers;

    public HelpersFragment() {
        // Required empty public constructor
    }

    public static HelpersFragment newInstance(String param1, String param2) {
        HelpersFragment fragment = new HelpersFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getActivity().setTitle("Helpers");
        root_view = inflater.inflate(R.layout.fragment_helpers, container, false);

        recyclerView = root_view.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);

        relativeLayoutNoHelpers = root_view.findViewById(R.id.relativeLayoutNoHelpers);

        mDb = FirebaseDatabase.getInstance();

        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);

        helpers = new ArrayList<>();
        fetch();

        mAdapter = new HelperAdapter(helpers);
        recyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();

        return root_view;
    }

    private void fetch(){
        DatabaseReference dbRef = mDb.getReference("posts");
        final FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        dbRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {
                boolean hasValues = false;
                helpers.clear();
                mAdapter.notifyDataSetChanged();
                for(DataSnapshot d : dataSnapshot.getChildren())
                    if(d.child("uid").getValue().toString().equals(currentUser.getUid())){
                        final String subject = d.child("subject").getValue().toString();
                        if(d.child("helpers").getValue() != null) {
                            hasValues = true;
                            for(final DataSnapshot helper : d.child("helpers").getChildren()) {
                               DatabaseReference refHelper = mDb.getReference("users").child(helper.getKey());
                               refHelper.addListenerForSingleValueEvent(new ValueEventListener() {
                                   @Override
                                   public void onDataChange(@NonNull DataSnapshot dataSnapshotUser) {
                                       helpers.add(new Helper(dataSnapshotUser.child("profilePicture").getValue().toString(),
                                               dataSnapshotUser.child("displayName").getValue().toString(),
                                               subject, dataSnapshotUser.getKey()));
                                       mAdapter.notifyDataSetChanged();
                                   }

                                   @Override
                                   public void onCancelled(@NonNull DatabaseError databaseError) {

                                   }
                               });
                            }
                        }
                    }

                if(hasValues) relativeLayoutNoHelpers.setVisibility(View.GONE);
                else          relativeLayoutNoHelpers.setVisibility(View.VISIBLE);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    public class HelperAdapter extends RecyclerView.Adapter<HelperAdapter.ViewHolder>{

        private List<Helper> mHelpers;

        public class ViewHolder extends RecyclerView.ViewHolder{

            CircularImageView image;
            TextView subject;
            TextView name;
            MaterialRippleLayout layout;

            public ViewHolder(View itemView){
                super(itemView);
                image = itemView.findViewById(R.id.image);
                subject = itemView.findViewById(R.id.subject);
                name = itemView.findViewById(R.id.name);
                layout = itemView.findViewById(R.id.layout);
            }
        }

        public HelperAdapter(List<Helper> myHelpers){
            mHelpers = myHelpers;
        }

        @NonNull
        @Override
        public HelperAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.item_helpers, parent, false);
            return new ViewHolder(listItem);
        }

        @Override
        public void onBindViewHolder(@NonNull HelperAdapter.ViewHolder holder, final int position) {
            final Helper helper = mHelpers.get(position);
            holder.subject.setText(helper.getSubject());
            if(getActivity() != null)
                Glide.with(getActivity()).load(helper.getProfilePhoto()).into(holder.image);
            holder.name.setText(helper.getName());
            holder.layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   Intent intent = new Intent(getActivity(), ProfileActivity.class);
                   intent.putExtra("uid", helper.getUid());
                   startActivity(intent);
                }
            });
        }

        @Override
        public int getItemCount() {
            return mHelpers.size();
        }

    }
}
