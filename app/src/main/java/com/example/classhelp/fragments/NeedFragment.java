package com.example.classhelp.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.example.classhelp.R;
import com.example.classhelp.model.Post;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.noowenz.customdatetimepicker.CustomDateTimePicker;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

public class NeedFragment extends Fragment {
    private View root_view;
    private Button btnDateTime, buttonSend;
    NeedFragment thisFragment;
    private TextView dateTimeTextView;
    private NumberPicker numberPicker;
    private String dateTimeSet = "";
    private DatabaseReference mDatabase;
    private TextInputEditText inputSubject, inputDetails;
    private String subject, details;
    private int hoursNeeded;

    public NeedFragment() {
    }

    public static NeedFragment newInstance(String param1, String param2) {
        NeedFragment fragment = new NeedFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        thisFragment = this;
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root_view = inflater.inflate(R.layout.fragment_need, container, false);
        btnDateTime = root_view.findViewById(R.id.buttonDateTime);
        dateTimeTextView = root_view.findViewById(R.id.textViewDateTime);
        numberPicker = root_view.findViewById(R.id.numberPicker);
        inputSubject = root_view.findViewById(R.id.inputSubject);
        inputDetails = root_view.findViewById(R.id.inputDetails);
        buttonSend = root_view.findViewById(R.id.buttonSend);

        getActivity().setTitle("Need help");

        String[] nums = new String[5];
        for(int i=0;i < nums.length;i++)
            nums[i] = Integer.toString(i+1);

        numberPicker.setMinValue(1);
        numberPicker.setMaxValue(5);
        numberPicker.setWrapSelectorWheel(false);
        numberPicker.setDisplayedValues(nums);
        numberPicker.setValue(1);
        mDatabase = FirebaseDatabase.getInstance().getReference("posts");

        btnDateTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomDateTimePicker customDateTimePicker = new CustomDateTimePicker(getActivity(), new CustomDateTimePicker.ICustomDateTimeListener() {
                    @SuppressLint("DefaultLocale")
                    @Override
                    public void onSet(Dialog dialog, Calendar calendar, Date date, int i, String s, String s1, int i1, int i2, String s2, String s3, int i3, int i4, int i5, int i6, String s4) {
                        dateTimeSet = String.format("%02d/%02d/%d %02d:%02d", i2, i1+1, i, i4, i5);
                        dateTimeTextView.setText(dateTimeSet);
                    }

                    @Override
                    public void onCancel() {

                    }
                });
               customDateTimePicker.set24HourFormat(true);
               Calendar futureCal = Calendar.getInstance();
               futureCal.add(Calendar.YEAR, 1);
               customDateTimePicker.setMaxMinDisplayDate(Calendar.getInstance().getTimeInMillis(), futureCal.getTimeInMillis());
               customDateTimePicker.setMaxMinDisplayedTime(10, Integer.MAX_VALUE);
               customDateTimePicker.showDialog();
            }
        });

        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hoursNeeded = numberPicker.getValue();
                subject = inputSubject.getText().toString();
                details = inputDetails.getText().toString();
                if(subject.equals("") || details.equals("") || dateTimeSet.equals("")){
                    Toast.makeText(getActivity(), "Please check the form.", Toast.LENGTH_SHORT).show();
                }else{
                    addToFirebase(hoursNeeded, subject, details);
                }
            }
        });
        return root_view;
    }

    private void addToFirebase(int hoursNeeded, final String subject, String details){
        String id = mDatabase.push().getKey();
        Post post = new Post(dateTimeSet, hoursNeeded, subject, details, ServerValue.TIMESTAMP);
        post.setUid(FirebaseAuth.getInstance().getCurrentUser().getUid());
        post.setInvertedTimestamp(-1 * new Date().getTime());
        mDatabase.child(id).setValue(post).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                inputDetails.setText("");
                inputSubject.setText("");
                dateTimeSet = "";
                dateTimeTextView.setText("Not set");
                Toast.makeText(getActivity(), "Post was added", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
